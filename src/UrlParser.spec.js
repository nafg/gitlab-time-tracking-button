/* eslint-disable func-names, prefer-arrow-callback */

import { describe, it } from 'mocha';
import { expect } from 'chai';

import UrlParser from './UrlParser';

describe('UrlParser', function () {
  it('parses group, projcet, and issue number', function () {
    const url = 'https://example.com:11/my-Group/my.Project/issues/5';
    const parser = new UrlParser(url);

    expect(parser.getPath()).to.have.ordered.members(['my-Group', 'my.Project']);
    expect(parser.getIssueNr()).to.equal('5');
    expect(parser.getAllData()).to.deep.equal({
      path: ['my-Group', 'my.Project'],
      instance: 'https://example.com:11/',
      issue: '5',
    });
  });

  // See issue #40: https://gitlab.com/adimit/gitlab-time-tracking-button/issues/40
  it('parses multiple groups, project, and issue number', function () {
    const url = 'https://example.com:11/myGroup/mySubGroup/mySubSubgroup/myProject/issues/6';
    const parser = new UrlParser(url);

    expect(parser.getPath()).to.have.ordered.members([
      'myGroup',
      'mySubGroup',
      'mySubSubgroup',
      'myProject',
    ]);
    expect(parser.getIssueNr()).to.equal('6');
  });

  it('parses instance when other stuff isn\'t defined', function () {
    const url = 'https://example.com:11/#myGroup/myProject/issues/5'; // everything after the hash is not the path!
    const parser = new UrlParser(url);

    expect(parser.getInstanceKey()).to.equal('https://example.com:11/');
    expect(parser.getPath()).to.be.null;
  });
});
